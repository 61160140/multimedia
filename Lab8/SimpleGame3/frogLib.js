function Frog() {
  tFrog = new Sprite(scene, './frog.png', 50, 50)
  //property
  tFrog.speed = 0
  tFrog.setSpeed(tFrog.speed)
  tFrog.setPosition(100, 150)
  tFrog.setAngle(0)

  //method
  tFrog.checkKeys = function () {
    if (keysDown[K_LEFT]) {
      this.changeAngleBy(-5)
    }
    if (keysDown[K_RIGHT]) {
      this.changeAngleBy(5)
    }
    if (keysDown[K_UP]) {
      if (this.speed < 10) {
        this.speed++
        // console.log(this.speed)
      } else {
        this.speed = 10
      }
    }
    if (keysDown[K_DOWN]) {
      if (this.speed > -3) {
        this.speed--
        // console.log(this.speed)
      } else {
        this.speed = -3
      }
    }
    tFrog.setSpeed(this.speed)
  }

  return tFrog
}

function Fly() {
  tFly = new Sprite(scene, "./fly.png", 20, 30)
  tFly.setSpeed(10)

  tFly.wriggle = function () {
    newDir = (Math.random() * 90) - 45
    this.changeAngleBy(newDir)
  }

  tFly.reset = function () {
    newX = Math.random()*this.cWidth
    newY = Math.random()*this.cHeight
    this.setPosition(newX,newY)
  }

  return tFly
}