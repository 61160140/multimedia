let number = 0;
let correctNum = document.getElementById('correct');
let againBtn = document.getElementById('againBtn');
let text = document.getElementById('txt');
let input = document.getElementById('guess');

function init(){
    number = Math.floor((Math.random()*100)+1);
    //correctNum.innerHTML = number;
    input.focus();
    againBtn.style.visibility = "hidden";
    text.innerHTML = "I'm thinking of a number between 0 and 100. Guess my number, and I'll tell if you are too high, too low or correct.";
}
init();

let i = 0;
function check(){
    let guessNum = input.value;
    i++;
    if (guessNum == number){
        text.innerHTML = i+") Correct!";
        againBtn.style.visibility = "visible";
    }else if (guessNum < number){
        text.innerHTML = i+") Too low";
    }else if (guessNum > number){
        text.innerHTML = i+") Too high";
    }
    document.getElementById('guess').value = "";
}

function again(){
    i = 0;
    init();
}