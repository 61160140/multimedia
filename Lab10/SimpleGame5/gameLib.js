function Car() {
  tCar = new Sprite(game, './car.png', 70, 50)
  tCar.setSpeed(0)
  tCar.setAngle(90)
  tCar.setPosition(100, 400)
  tCar.changeImage('./car.png')

  tCar.checkKeys = function () {
    tCar.changeImage('./car.png')
    if (keysDown[K_UP]) {
      this.turnBy(-10)
    }
    if (keysDown[K_DOWN]) {
      this.turnBy(10)
    }
    if (keysDown[K_SPACE]) {
      tCar.changeImage('./redCar.png')
      car.fire()
    }
  }

  tCar.fire = function () {
    missile = new Missile()
    tMissile.fire()
  }

  return tCar
}

function Missile() {
  tMissile = new Sprite(game, './missile.png', 30, 20)
  tMissile.hide()

  tMissile.fire = function () {
    this.show()
    this.setPosition(car.x, car.y)
    this.setAngle(car.getImgAngle())
    this.setSpeed(15)
    this.setBoundAction(DIE)
  }

  return tMissile
}

function HoverCar() {
  tHover = new Sprite(game, './hoverCar.png', 70, 50)
  tHover.setSpeed(10)
  tHover.setPosition(700, (Math.random() * 500) + 50)
  tHover.setMoveAngle(270)
  console.log(tHover.y)

  tHover.reset = function () {
    newY = Math.random() * this.cHeight
    this.setPosition(800, newY)
  }

  return tHover
}